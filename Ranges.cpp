#include "Ranges.h"
#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>
#include <cmath>

constexpr uint64_t RANDOM_ABORT_AFTER_TRIES = 1000000;

Ranges::Ranges() : total_packets(0), killed_packets(0) {
  std::random_device rd;
  uint64_t seed = rd();
  gen.seed(seed);
  std::cout << "Seed: " << seed << "\n";
}

void Ranges::readRangesFromFile(const std::string &filename) {
  std::ifstream killrange_file;
  killrange_file.open(filename);
  if (killrange_file.fail()) {
    throw std::runtime_error("Failed to open killrange file: " + std::string(strerror(errno)));
  }
  std::string line;
  int32_t linecount = 0;
  while (getline(killrange_file, line)) {
    linecount++;
    std::istringstream iss(line);
    size_t start;
    size_t end;
    if (!(iss >> start >> end)) {
      throw std::runtime_error("Error while parsing killrange file in line " + std::to_string(linecount));
    }
    if (start > end || start < 0) {
      std::cerr << "Invalid killrange in line " << linecount << ". (start > end or start < 0)\n";
      continue;
    }
    addRange(start, end);
  }
  // Distribution generates values in [a,b]
  range_distribution = std::make_unique<std::uniform_int_distribution<uint64_t>>(0, ranges.size() - 1);
}

void Ranges::addRange(size_t start, size_t end) {
  size_t range_length = end - start + 1;
  uint64_t packets_in_range = std::ceil((double) range_length / PACKET_SIZE);
  ranges.emplace_back(start, end);
  range_packet_map.emplace_back(packets_in_range, false);
  // Distribution generates values in [a,b]
  range_packet_distributions.emplace_back(0, packets_in_range - 1);
  total_packets += packets_in_range;
}

Range Ranges::getRandomPacket() {
  uint64_t tries = 0;
  while (tries < RANDOM_ABORT_AFTER_TRIES) {
    uint64_t range_idx = range_distribution->operator()(gen);
    uint64_t packet_idx = range_packet_distributions[range_idx](gen);
    if (!range_packet_map[range_idx][packet_idx]) {
      range_packet_map[range_idx][packet_idx] = true;
      size_t start = ranges[range_idx].first + packet_idx * PACKET_SIZE;
      size_t end = start + PACKET_SIZE - 1;
      if (end > ranges[range_idx].second) {
        end = ranges[range_idx].second;
      }
      killed_packets++;
      return {start, end};
    }
    tries++;
  }
  throw std::logic_error(
      "Could not find an undeleted packet after " + std::to_string(RANDOM_ABORT_AFTER_TRIES) + " tries.");
}

