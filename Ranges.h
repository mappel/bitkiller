#ifndef BITKILLER__RANGES_H_
#define BITKILLER__RANGES_H_

#include <vector>
#include <unordered_map>
#include <random>
#include <memory>

typedef std::pair<size_t, size_t> Range;
typedef std::vector<Range> RangeVector;
constexpr size_t PACKET_SIZE = 1330;

class Ranges {
 public:
  Ranges();
  explicit Ranges(uint64_t seed) : gen(seed), total_packets(0), killed_packets(0) {}
  void readRangesFromFile(const std::string &filename);
  const RangeVector &getRanges() { return ranges; }
  void incKilledPackets() { killed_packets++; }
  Range getRandomPacket();
  // Returns the percentage (in interval [0,1]) of packets that where returned by getRandomPacket().
  double getKillPercentage() { return (1.0 / total_packets) * killed_packets; }

 private:
  std::mt19937_64 gen;
  RangeVector ranges;
  // Uses the same order as the ranges vector. For each range, contains a vector of length packets_in_range. For each
  // packet, a boolean keeps track if the packet was already killed. true == killed.
  std::vector<std::vector<bool>> range_packet_map;
  // Provides a uniform distribution over the interval [0, range_packet_map.size() - 1]
  std::unique_ptr<std::uniform_int_distribution<uint64_t>> range_distribution;
  // Uses the same order as the ranges vector. For each range i, contains a uniform distribution over the interval
  // [0, range_packet_map[i].size() - 1]
  std::vector<std::uniform_int_distribution<uint64_t>> range_packet_distributions;
  uint64_t total_packets;
  uint64_t killed_packets;

  // Adds the range to the ranges vector and initializes boolean vector with correct size in range_packet_map as well
  // as corresponding uniform distribution in range_packet_distributions. The number of packets in a range is defined by
  // ceil(range_len / PACKET_SIZE), i.e., if the range is not divisible by PACKET_SIZE, the remainder is still treated
  // as a complete packet.
  void addRange(size_t start, size_t end);

};

#endif //BITKILLER__RANGES_H_
