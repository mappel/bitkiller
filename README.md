# Bitkiller: Fast file corruption for the masses

Bitkiller is a small tool with the sole purpose of creating a corrupt
copy of a file that has some byte(s) corrupted (i.e., zeroed).
```
./bitkiller <killrange-file> <input> <output> [--mode <normal|uniform|burst>] [--loss <packet-loss>] [--burst <correlation>] [--seed <seed>] [-q]
```
The killrange file has a simple structure:
```
<start range 0> <end range 0>
<start range 1> <end range 1>
...
```
Each line corresponds to a byte range that should be corrupted. The
bytestream starts at 0 and ranges are inclusive, i.e., the range `0 1`
would corrupt the first two bytes. To corrupt a single byte, specify the
same number twice.

The input file needs to exist and can be any kind of file. The output
file is either created or overwritten, if it exists.

### Options
The packet size that is used for the loss modes is specified as `PACKET_SIZE` in `Ranges.h` (currently 1330 bytes).
```
--mode <normal|uniform|burst> [default: normal]
```
* `normal`: Overwrites all specified ranges.
* `uniform`: Select packets from the ranges uniformly random and overwrites them until the specified loss rate
(specified by `--loss`) is reached.
* `burst`: Applies the Simplified Gilbert-Elliot (SGE) loss model on a per-packet basis to the ranges. The loss rate
(`--loss`) and burst length (as correlation `--burst`) can be specified.  
**NOTE: This mode does not *guarantee* that the specified loss rate is reached. This will/should maybe changed.**

```
--loss <packet-loss> [default: 0]
```
Specifies the loss rate for `uniform` and `burst` modes. Needs to be in range [0,1] and needs to be specified if a loss
mode was selected. 

```
--burst <correlation> [default: 0]
```
Specifies to correlation (and thus the expected burst length) for the SGE model. Needs to be in range [-1,1].

-1 means that there are only state transitions (burst length always equal to 1).

0 means no correlation (burst lengths are random).
 
1 means full correlation so no state transitions.

Choosing a value closer to 1 increases the burst length, while closer to -1 decreases the burst length. The expected
value of the burst length can be calculated by
```
    1
----------
(1-c)(1-e)
```
where `c` is the correlation and `e` is the packet loss.

```
--seed <seed> [default: None]
```
If this parameter is not specified, the loss modes generate a new random seed (that is also printed to stdout). To
reproduce the same results, the seed can be specified as a parameter.

```
-q
```
If present, mutes all output to stdout. Errors are still printed to stderr.

## Build
Like with all CMake projects, run the following from the source
directory:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```