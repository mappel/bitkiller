#include <iostream>
#include <sys/stat.h>
#include <sys/mman.h>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include "Ranges.h"

enum LOSS_MODE {
  NORMAL,
  UNIFORM,
  BURST
};

int main(int32_t argc, char **argv) {
  LOSS_MODE mode = NORMAL;
  // Needs to be in range [0,1].
  double packet_loss = 0;
  // Needs to be in range [-1,1].
  // -1 means that there are only state transitions (burst length always equal to 1).
  // 0 means no correlation (burst lengths are random).
  // 1 means full correlation so no state transitions. Model stays in the starting state (GOOD in our case so no loss).
  // Choosing a value closer to 1 increases the burst length, while closer to -1 decreases the burst length.
  double correlation = 0;
  bool seed_specified = false;
  uint64_t seed = 0;
  bool quiet = false;
  std::string mode_parameter = "--mode";
  std::string packet_loss_parameter = "--loss";
  std::string burst_parameter = "--burst";
  std::string seed_parameter = "--seed";
  std::string quiet_parameter = "-q";
  if (argc < 4) {
    std::cout << "usage: " << argv[0]
              << " <killrange-file> <input> <output> [--mode <normal|uniform|burst>] [--loss <packet-loss>] [--burst <correlation>] [--seed <seed>] [-q]\n";
    return 1;
  }
  std::string killrange_file = argv[1];
  std::string input_file = argv[2];
  std::string output_file = argv[3];
  for (int32_t i = 4; i < argc; i++) {
    std::string next_arg = argv[i];
    if (!next_arg.compare(0, next_arg.size(), mode_parameter) && i + 1 < argc) {
      std::string mode_parameter_value = std::string(argv[i + 1]);
      if (mode_parameter_value == "normal") {
        mode = NORMAL;
      } else if (mode_parameter_value == "uniform") {
        mode = UNIFORM;
      } else if (mode_parameter_value == "burst") {
        mode = BURST;
      } else {
        std::cerr << "Invalid mode: " << mode_parameter_value << "\n";
        return 1;
      }
      i++;
    } else if (!next_arg.compare(0, next_arg.size(), packet_loss_parameter) && i + 1 < argc) {
      packet_loss = std::stod(argv[i + 1]);
      i++;
      if (packet_loss > 1 || packet_loss < 0) {
        std::cerr << "Packet loss needs to be in range [0,1]\n";
        return 1;
      }
    } else if (!next_arg.compare(0, next_arg.size(), burst_parameter) && i + 1 < argc) {
      correlation = std::stod(argv[i + 1]);
      i++;
      if (correlation > 1 || correlation < -1) {
        std::cerr << "Burst correlation needs to be in range [-1,1]\n";
        return 1;
      }
    } else if (!next_arg.compare(0, next_arg.size(), seed_parameter) && i + 1 < argc) {
      seed_specified = true;
      seed = std::stoull(argv[i + 1]);
      i++;
    } else if (!next_arg.compare(0, next_arg.size(), quiet_parameter)) {
      quiet = true;
    } else {
      std::cerr << "Unkown parameter or missing argument: " << argv[i] << "\n";
      return 1;
    }
  }
  if ((mode == UNIFORM || mode == BURST) && packet_loss == 0) {
    std::cerr << "Loss modes require --loss parameter.\n";
    return 1;
  }
  if (quiet) {
    std::cout.setstate(std::ios_base::failbit);
  }
  if (packet_loss > 0) {
    std::cout << "Setting packet loss to " << packet_loss << "\n";
  }
  std::unique_ptr<Ranges> killranges;
  if (seed_specified) {
    killranges = std::make_unique<Ranges>(seed);
  } else {
    killranges = std::make_unique<Ranges>();
  }
  std::cout << "Reading kill ranges... ";
  killranges->readRangesFromFile(killrange_file);
  std::cout << "OK\n";
  std::cout << "Preparing files... ";
  struct stat st{};
  if (stat(input_file.c_str(), &st) < 0) {
    std::cerr << "Error while getting input file size: " << strerror(errno) << "\n";
    return 1;
  }
  size_t input_file_size = st.st_size;
  int32_t input_fd = open(input_file.c_str(), O_RDONLY);
  if (input_fd < 0) {
    std::cerr << "Failed to open input file: " << strerror(errno) << "\n";
    return 1;
  }
  int32_t output_fd = open(output_file.c_str(), O_RDWR | O_CREAT, 0664);
  if (output_fd < 0) {
    std::cerr << "Failed to open output file: " << strerror(errno) << "\n";
    close(input_fd);
    return 1;
  }
  if (ftruncate(output_fd, input_file_size) < 0) {
    std::cerr << "Failed to set output file size: " << strerror(errno) << "\n";
    close(input_fd);
    close(output_fd);
    return 1;
  }
  auto *input_mmap =
      static_cast<uint8_t *>(mmap(nullptr, input_file_size, PROT_READ, MAP_PRIVATE | MAP_POPULATE, input_fd, 0));
  if (input_mmap == MAP_FAILED) {
    std::cerr << "Failed to mmap input file: " << strerror(errno) << "\n";
    close(input_fd);
    close(output_fd);
    return 1;
  }
  close(input_fd);
  if (madvise(input_mmap, input_file_size, MADV_SEQUENTIAL) < 0) {
    std::cerr << "madvise: " << strerror(errno) << "\n";
  }
  auto *output_mmap = static_cast<uint8_t *>(mmap(nullptr, input_file_size, PROT_WRITE, MAP_SHARED, output_fd, 0));
  if (output_mmap == MAP_FAILED) {
    std::cerr << "Failed to mmap output file: " << strerror(errno) << "\n";
    munmap(input_mmap, input_file_size);
    close(output_fd);
    return 1;
  }
  close(output_fd);
  std::cout << "OK\n";
  std::cout << "Copying input to output... ";
  memcpy(output_mmap, input_mmap, input_file_size);
  std::cout << "OK\n";
  if (munmap(input_mmap, input_file_size) < 0) {
    std::cerr << "Failed to munmap input file: " << strerror(errno) << "\n";
  }
  std::cout << "Killing bits in output file...\n";
  switch (mode) {
    case NORMAL:
      for (auto &killrange : killranges->getRanges()) {
        std::cout << "Killing range " << killrange.first << "-" << killrange.second << "... ";
        if (killrange.second + 1 > input_file_size) {
          std::cout << "ERROR\n";
          std::cerr << "End of killrange is after EOF.\n";
          continue;
        }
        memset(output_mmap + killrange.first, 0, killrange.second - killrange.first + 1);
        std::cout << "OK\n";
      }
      break;
    case UNIFORM:
      while (killranges->getKillPercentage() < packet_loss) {
        Range killrange = killranges->getRandomPacket();
        std::cout << "Killing range " << killrange.first << "-" << killrange.second << "... ";
        if (killrange.second + 1 > input_file_size) {
          std::cout << "ERROR\n";
          std::cerr << "End of killrange is after EOF.\n";
          continue;
        }
        memset(output_mmap + killrange.first, 0, killrange.second - killrange.first + 1);
        std::cout << "OK. current loss: " << killranges->getKillPercentage() << "\n";
      }
      break;
    case BURST:
      // The burst mode simulates burst losses via the Simplified Gilbert-Elliot model. It consists of a Markov chain
      // with two states: G (good) and B (bad). The transition probabilities are 1 - beta for G->B and 1 - alpha for
      // B->G.
      // The model starts in state G. For each packet, first the current state is checked. If the model is in state B,
      // the packet is dropped. Then, a random number in [0,1] is generated and a state transition performed if
      // applicable.
      double alpha = correlation + packet_loss * (1 - correlation);
      double beta = 1 - packet_loss * (1 - correlation);
      if (!seed_specified) {
        std::random_device rd;
        seed = rd();
        std::cout << "Seed: " << seed << "\n";
      }
      std::cout << "Alpha: " << alpha << " Beta: " << beta << "\n";
      std::mt19937_64 gen(seed);
      // Generate in interval [0,1] not [0,1)
      // see notes of https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
      std::uniform_real_distribution<double> dis(0, std::nextafter(1.0, std::numeric_limits<double>::max()));
      uint64_t burst_length = 0;
      bool good = true;
      for (auto &killrange : killranges->getRanges()) {
        for (size_t offset = killrange.first; offset <= killrange.second; offset += PACKET_SIZE) {
          double prob = dis(gen);
          if (good) {
            burst_length = 0;
            if (prob > beta) {
              good = false;
            }
          } else {
            if (prob > alpha) {
              good = true;
            }
            size_t offset_end = offset + PACKET_SIZE - 1;
            if (offset_end > killrange.second) {
              offset_end = killrange.second;
            }
            burst_length++;
            killranges->incKilledPackets();
            std::cout << "Killing range " << offset << "-" << offset_end << "... ";
            if (offset_end + 1 > input_file_size) {
              std::cout << "ERROR\n";
              std::cerr << "End of killrange is after EOF.\n";
              continue;
            }
            memset(output_mmap + offset, 0, offset_end - offset + 1);
            std::cout << "OK. Burst length: " << burst_length << "\n";
          }
        }
      }
      std::cout << "Effective loss: " << killranges->getKillPercentage() << "\n";
      break;
  }
  std::cout << "Success!\n";
  if (munmap(output_mmap, input_file_size) < 0) {
    std::cerr << "failed to munmap output file: " << strerror(errno) << "\n";
  }
  return 0;
}